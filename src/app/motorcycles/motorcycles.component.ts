import { Component } from '@angular/core';
import { MotorcyclesService } from './motorcycles.service';
import { Motorcycle } from './motorcycles.types';

@Component({
  selector: 'app-motorcycles',
  templateUrl: './motorcycles.component.html',
  styleUrls: ['./motorcycles.component.scss']
})
export class MotorcyclesComponent {

  motorcyclesInfo: Motorcycle[] = [];

  constructor(private motorcyclesService: MotorcyclesService) { }

  ngOnInit(): void {
    this.motorcyclesService.getMotorcyclesInformation().subscribe(motorcycles => this.motorcyclesInfo = motorcycles);
  }

}
