export interface Motorcycle {
    make: string;
    model: string;
    year: string;
    type: string;
  }
  