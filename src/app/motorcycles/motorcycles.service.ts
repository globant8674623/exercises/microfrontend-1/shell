import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { Motorcycle } from './motorcycles.types';

@Injectable({
  providedIn: 'root'
})
export class MotorcyclesService {

  private motorcycles: Motorcycle[] = [
    {
      make: 'Honda',
      model: 'CBR500R',
      year: '2022',
      type: 'Sport'
    },
    {
      make: 'Yamaha',
      model: 'MT-07',
      year: '2022',
      type: 'Naked'
    },
    {
      make: 'Kawasaki',
      model: 'Ninja 650',
      year: '2022',
      type: 'Sport'
    },
    {
      make: 'Suzuki',
      model: 'GSX-S750',
      year: '2022',
      type: 'Naked'
    }
  ];

  getMotorcyclesInformation(): Observable<Motorcycle[]> {
    return of(this.motorcycles);
  }
}
